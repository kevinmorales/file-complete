#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#define LENGTH 128 //Number of ASCII characters

//I made a macros definition so that I could use the number of ASCII characters as a global constant

/*
Below is a struct that represents a single node in the linked list. It only has a character pointer
and a pointer to the next node. Since we are only inserting and searching linearly, I did not see
a reason for pointer to the last node as is common practice in linked list implementations. 
There are no classes in C so a struct is necessary for a linked list. 
*/
struct node
{
    char *fileName;
    struct node *next;
};

/*I like function declarations before the main method, I come from Java and so I am used to 
the main method going before the methods/functions
*/
void printList(struct node **);

void searchAndPrint(char *, struct node **, int);

void addNode(char *, struct node **);

int main()
{

    /*
    I like to do a local constants, local variables section before I start the program, 
    especially in C, in my opinion. Any variable or constant I use in the main function 
    is declared before any actual code is written. 
    I only allow the user to enter 50 characters for directory lengths and for file starting
    strings. 

    struct node *arrayOfLinkedLists[LENGTH];
    This is an array storing 128 "stuct node" type "objects"
    Each array index holds the a pointer to the head of a linked list that corresponds to 
    a single ASCII character.

    */
    //local constants
    const int MAX_LENGTH = 50;

    //local variables
    DIR *dir;
    struct dirent *files;
    struct node *arrayOfLinkedLists[LENGTH];
    char folderName[MAX_LENGTH];
    char fileStart[MAX_LENGTH];
    int index = 0;
    int finished = 1;
    int counter = 0;
    int c;
    /****************************************/

    /*
    Before we can add any nodes to any linked list in the array, we need to initialize
    the head of the linked lists to NULL. There are 128 heads in the array so a simple
    for loop will get this done. 
    */
    //Initialize array of linked lists
    for (int i = 0; i < LENGTH; i++)
    {
        arrayOfLinkedLists[i] = NULL;
    } //END FOR

    /*I like to use this method for getting user input. A while loop with two 
    conditionals. Keep storing characters in our array so long as we have not 
    reached the capacity of the array minus one AND the user doesn't press the
    enter key. This ensures that there is no buffer overflow and also that
    the enter key is not stored in the string. Then we null terminate it to make
    sure the array is read as a string. This code is repeated later in the program
    to get user input if the user enters and invalid directory name. 
    */
    //Prompt user for a directory name
    printf("\nEnter a directory name: ");
    while ((c = getchar()) != '\n' && counter < MAX_LENGTH)
    {
        //if(counter < 10)
        folderName[counter] = c;
        counter++;
    }
    //Null terminate the string
    folderName[counter] = '\0';

    /*
    Below is a loop to ensure that a user enters a valid directory 
    the flag is only tripped when a valid directory has been entered
    and read. 
    */
    //WHILE the user is not finished entering a directory name
    while (finished != 0)
    {
        //IF the directory exists
        if ((dir = opendir(folderName)) != NULL)
        {
            //WHILE there are files to read
            while ((files = readdir(dir)) != NULL)
            {
                /*This grabs the first character of the file name and converts to an integer
                so that we can access the correct array index based on that value
                */
                //Get the value of the first character of the file name
                index = (int)(files->d_name)[0];

                /*
                files->d_name means we are accessing the files struct's data member called d_name
                which in this case is the name of the file*/
                //Call function to add the file name to the linked list
                addNode(files->d_name, &arrayOfLinkedLists[index]);

            } //END WHILE

            //Close the directory
            closedir(dir);
            finished = 0;
        }
        //ELSE there is no directory with that name
        else
        {
            //Let user know their directory does not exist
            printf("ERROR: %s\n", folderName);
            printf("No such directory exists. Try again.\n");

            //Prompt user for another directory name
            counter = 0;
            printf("\nEnter a directory name: ");
            while ((c = getchar()) != '\n' && counter < MAX_LENGTH)
            {
                //if(counter < 10)
                folderName[counter] = c;
                counter++;
            }
            //Null terminate the string
            folderName[counter] = '\0';

        } //END IF

    } //END WHILE

    /*
    This is for printing out the entire data structure. This is for testing purposes,
    If you want to make sure the entire data structure is properly populated, uncomment
    this block of code. 

    for (int w = 0; w < LENGTH; w++)
    {

        //IF the head of this linked list is NULL
        if (arrayOfLinkedLists[w] != NULL)
        {

            printList(&arrayOfLinkedLists[w]);
        }

    } //END FOR

    */

    /*Repeating a repurposed version of the code to read the directory name, now for reading the
   file starting string. We will loop until the user enters an empty string. */

    counter = 0;
    printf(">");
    while ((c = getchar()) != '\n' && counter < MAX_LENGTH)
    {
        fileStart[counter] = c;
        counter++;
    }
    fileStart[counter] = '\0';

    /*While the user did not enter an empty string*/

    while (strlen(fileStart) > 0)
    {
        //Get the value of the first character of the file name
        index = (int)(fileStart)[0];

        //IF the head of this linked list is NULL
        if (arrayOfLinkedLists[index] == NULL)
        {
            //Let the user know there are no files matching the string description
            printf("No files matching desciption \"%s\"\n", fileStart);
        }
        //ELSE this linked list has nodes in it
        else
        {
            //Call the function to search and print files
            searchAndPrint(fileStart, &arrayOfLinkedLists[index], (strlen(fileStart) - 1));
        }

        //GET MORE USER INPUT
        counter = 0;
        //Prompt for beginning of file names
        printf(">");
        while ((c = getchar()) != '\n' && counter < MAX_LENGTH)
        {
            fileStart[counter] = c;
            counter++;
        }
        fileStart[counter] = '\0';

    } //END WHILE

    printf("\n");

    return 0;
}

/*To call this function to add a new node to a linked list, we need to pass the 
name of the file we want to add and also the head of the linked list to which we
are adding it to. The head is sent as a double pointer, this is because the head of 
the linked list is a pointer to a struct, so when we pass it to the function, we are
sending a pointer that points to the pointer, which points to the struct. Difficult to
grasp at first. */

void addNode(char *name, struct node **head)
{
    struct node *current;
    char *temp;

    /********************************/

    /*Must handle two cases, the head of the linked list is NULL, so we have to 
    make the file the head of the linked list OR the head exists, so we must traverse
    to the end of the linked list*/

    //IF the head of this linked list is NULL
    if (*head == NULL)
    {
        //Allocate enough memory for a node
        *head = (struct node *)malloc(sizeof(struct node));

        //Set the node's file name to the name parameter
        (*head)->fileName = name;

        //Set the next attribute to NULL
        (*head)->next = NULL;
    }
    //ELSE this linked list has nodes in it
    else
    {
        current = (*head);

        while (current->next != NULL)
        {

            /*Comparison for sorting alphabetically*/
            if (strcmp((current->fileName), name) < 0)
            {

                temp = (current)->fileName;

                (current)->fileName = name;

                name = temp;

            } //END IF

            //Get the next node in the linked list
            current = current->next;

        } //END WHILE

        //Now that we are at the end of the linked list we can add a new node
        //Allocate memory for a new node
        current->next = (struct node *)malloc(sizeof(struct node));
        current->next->fileName = name;
        current->next->next = NULL;
    } //END IF

} //END addNode

/*This traverses the linked list and prints all nodes
in the linked list*/

void printList(struct node **head)
{
    //local constants

    //local variables
    struct node *current;

    /*********************/

    //Set the current node to the head
    current = (*head);
    printf("%s\n", (*head)->fileName);

    //WHILE not at the end of the list
    while (current->next != NULL)
    {
        //Get next node
        current = current->next;

        //Print the file name
        printf("%s\n", current->fileName);

    } //END WHILE
}

/*To call this function to search a linked list, we need the head of a linked list,
 the string the user entered, and a flag to indicate whether the user wants to print 
 all files in the linked list. */

void searchAndPrint(char *start, struct node **head, int wholeList)
{
    //local constants

    //local variables
    struct node *current;
    int i = 0;

    /*********************/

    //Set the current node to the head
    current = (*head);

    //IF the passes in int is 0, then that means the user want to print the whole list
    //This happens when a user enters just one character
    if (wholeList)
    {
        printf("%s\n", (*head)->fileName);

        while (current->next != NULL)
        {
            current = current->next;

            printf("%s\n", current->fileName);

        } //END WHILE
    }
    /*Otherwise, we need to check if the file names match the string description
    that the user specified in their input*/
    else
    {
        //ONLY compare the number of characters equal to the length of the user input
        if (strncmp(((*head)->fileName), start, strlen(start)) == 0)
        {
            printf("%s\n", (*head)->fileName);
        }

        //Traversal of the linked list
        while (current->next != NULL)
        {
            //Get the next node
            current = current->next;

            //ONLY compare the number of characters equal to the length of the user input
            if (strncmp(((current)->fileName), start, strlen(start)) == 0)
            {
                printf("%s\n", current->fileName);
                i++;
            } //END IF

        } //END WHILE

        //IF we never printed anything, then there were no files that matched the user input
        if (i == 0)
        {
            //Let the user know their search returned nothing
            printf("No files matching desciption \"%s\"\n", start);
        }
    }

} //END searchAndPrint
